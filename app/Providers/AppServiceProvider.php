<?php

namespace App\Providers;

use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->activeQueryDebug();
    }

    public function boot()
    {
        //
    }

    private function bindingType($value)
    {
        if (!is_numeric($value))
            $value = "'{$value}'";

        return $value;
    }

    private function activeQueryDebug()
    {
        if (env('QUERY_DEBUG', false)) {
            $maxSize = 2000000; // ~2Mb
            $nameFix = 'sql-log/log-' . date('Y-m-d');
            $name = "{$nameFix}.sql";
            $index = 0;
            while (\Storage::disk('local')->exists($name)
                && \Storage::disk('local')->size($name) >= $maxSize) {
                $index += 1;
                $name = "{$nameFix}-{$index}.sql";
            }

            \Event::listen(QueryExecuted::class,
                function (QueryExecuted $query) use ($name) {
                    $binding = $query->bindings;
                    $binding = array_map([$this, 'bindingType'], $binding);
                    $boundSql = str_replace(['%', '?'], ['%%', '%s'], $query->sql);
                    $boundSql = vsprintf($boundSql, $binding);
                    $sql = "-- " . date('d-m-Y H:i:s') . " Time: ({$query->time})\r\n ";
                    $sql .= $boundSql;
                    $sql .= ';';
                    \Storage::disk('local')->append($name, $sql);
                }
            );
        }
    }
}
