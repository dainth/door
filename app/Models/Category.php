<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends \Eloquent
{
    use SoftDeletes;

    protected $table = 'categories';
    protected $fillable = ['name', 'icon', 'image', 'status', 'category_id', 'slug'];
    protected $hidden = ['updated_at', 'deleted_at'];

    public function product()
    {
        return $this->hasMany(Product::class, 'category_id', 'id');
    }

    static function getCategories($name, $status)
    {
        return self::when($name, function ($qr) use ($name) {
            $qr->where('name', 'like', "%$name%");
        })
            ->when($status, function ($qr) use ($status) {
                $qr->where('status', $status);
            })
            ->latest()
            ->paginate(PAGINATE);
    }

    static function getParentCategories()
    {
        return self::whereNull('category_id')->get();
    }

    static function getCategoriesInProduct($name)
    {
        return self::when($name, function ($qr) use ($name) {
            $qr->where('name', 'like', "%$name%");
        })
            ->latest()
            ->get(['name', 'id']);
    }
}
