<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends \Eloquent
{
    use SoftDeletes;

    protected $table = 'products';
    protected $fillable = ['category_id', 'material_id', 'name', 'slug', 'length', 'width', 'height', 'weight', 'price', 'image', 'content'];
    protected $hidden = ['updated_at', 'deleted_at'];

    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function materials()
    {
        return $this->belongsToMany(Material::class, OptionProduct::class, 'product_id', 'material_id');
    }

    public function colors()
    {
        return $this->belongsToMany(Color::class, OptionProduct::class, 'product_id', 'color_id');
    }

    public function optionProduct()
    {
        return $this->hasMany(OptionProduct::class, 'product_id', 'id');
    }

    static function getProducts($name, $category_id)
    {
        return self::when($name, function ($qr) use ($name) {
            $qr->where('name', 'like', "%$name%");
        })
            ->when($category_id, function ($qr) use ($category_id) {
                $qr->whereCategory_id($category_id);
            })
            ->latest()->paginate(PAGINATE);
    }
}
