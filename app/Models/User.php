<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    use SoftDeletes;

    protected $table = 'users';
    protected $fillable = ['name', 'birth', 'phone', 'address', 'gender', 'status', 'image', 'email', 'password'];
    protected $hidden = ['password', 'remember_token', 'email_verified_at', 'updated_at', 'deleted_at'];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    static function getUsers($name, $phone, $email, $status)
    {
        return self::when($name, function ($qr) use ($name) {
            $qr->where('name', 'like', "%$name%");
        })
            ->when($phone, function ($qr) use ($phone) {
                $qr->wherePhone($phone);
            })
            ->when($email, function ($qr) use ($email) {
                $qr->where('email', 'like', "%$email%");
            })
            ->when($status, function ($qr) use ($status) {
                $qr->whereStatus($status);
            })
            ->latest()
            ->paginate(PAGINATE);
    }
}
