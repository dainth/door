<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Material extends \Eloquent
{
    use SoftDeletes;

    protected $table = 'materials';
    protected $fillable = ['name'];
    protected $hidden = ['updated_at', 'deleted_at'];

    public function product()
    {
        return $this->hasMany(Product::class, 'material_id', 'id');
    }

    static function getMaterials($name)
    {
        return self::when($name, function ($qr) use ($name) {
            $qr->where('name', 'like', "%$name%");
        })->latest()->paginate(PAGINATE);
    }

    static function getMaterialsInProduct($name)
    {
        return self::when($name, function ($qr) use ($name) {
            $qr->where('name', 'like', "%$name%");
        })
            ->latest()
            ->get(['name', 'id']);
    }
}
