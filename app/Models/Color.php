<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Color extends \Eloquent
{
    use SoftDeletes;

    protected $table = 'colors';
    protected $fillable = ['name', 'code'];
    protected $hidden = ['updated_at', 'deleted_at'];

    public function products()
    {
        return $this->belongsToMany(Product::class, OptionProduct::class, 'product_id', 'color_id');
    }

    static function getColors($name)
    {
        return self::when($name, function ($qr) use ($name) {
            $qr->where('name', 'like', "%$name%");
        })->latest()->paginate(PAGINATE);
    }

    static function getColorsInProduct($name)
    {
        return self::when($name, function ($qr) use ($name) {
            $qr->where('name', 'like', "%$name%");
        })
            ->latest()
            ->get(['name', 'id']);
    }
}
