<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class OptionProduct extends \Eloquent
{
    protected $table = 'option_products';
    protected $fillable = ['product_id', 'color_id', 'material_id', 'image', 'price'];
    public $timestamps = false;

    public function colors()
    {
        return $this->belongsTo(Color::class, 'color_id', 'id');
    }

    public function materials()
    {
        return $this->belongsTo(Material::class, 'material_id', 'id');
    }
}
