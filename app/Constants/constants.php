<?php
define('INACTIVE', 0);
define('ACTIVE', 1);
define('TWO', 2);
define('EIGHT', 8);
define('TEN', 10);
define('FIFTEEN', 15);
define('CATEGORIES', 'categories');
define('PRODUCTS', 'products');
define('USERS', 'users');
define('DEFAULT_EXTENSION', '.png');

define('PAGINATE', 15);
