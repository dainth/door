<?php

use Illuminate\Support\Str;

if (!function_exists('upload_image')) {
    function uploadImage($path, $file)
    {
        $name = 'files/' . $path . '/' . now()->format('H-i-s-m-s-d-m-Y-') . Str::lower(Str::random(TEN)) . DEFAULT_EXTENSION;
        $file->move(public_path() . '/files/' . $path . '/', $name);
        return $name;
    }
}

if (!function_exists('file_url')) {
    function fileUrl($path, $image)
    {
        return '/files/' . $path . '/' . $image;
    }
}
