<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class CategoryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if ($this->id) {
            return [
                'name' => 'required|max:255|unique:categories,name,' . $this->id . ',id,deleted_at,NULL',
            ];
        } else {
            return [
                'name' => 'required|max:255|unique:categories,name,NULL,id,deleted_at,NULL',
            ];
        }
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập tên',
            'name.max' => 'Tên quá dài',
            'name.unique' => 'Tên đã được sử dụng',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json($validator->errors()->toArray(), Response::HTTP_BAD_REQUEST)
        );
    }
}
