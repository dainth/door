<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if ($this->profile && $this->id) {
            return [
                'name' => 'required|max:255',
                'address' => 'required|max:255',
                'email' => 'required|max:255|unique:users,email,' . $this->id . ',id,deleted_at,NULL',
            ];
        } else {
            if ($this->id) {
                return [
                    'name' => 'required|max:255',
                    'address' => 'required|max:255',
                    'email' => 'required|max:255|unique:users,email,' . $this->id . ',id,deleted_at,NULL',
                ];
            } else {
                return [
                    'name' => 'required|max:255',
                    'address' => 'required|max:255',
                    'email' => 'required|max:255|unique:users,email,NULL,id,deleted_at,NULL',
                    'password' => 'required|min:6|max:255'
                ];
            }
        }
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập tên',
            'name.max' => 'Tên quá dài',
            'address.required' => 'Vui lòng nhập địa chỉ',
            'address.max' => 'Địa chỉ quá dài',
            'email.required' => 'Vui lòng nhập email',
            'email.max' => 'Email quá dài',
            'email.unique' => 'Email đã được sử dụng',
            'password.required' => 'Vui lòng nhập mật khẩu',
            'password.min' => 'Mật khẩu quá ngắn',
            'password.max' => 'Mật khẩu quá dài',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json($validator->errors()->toArray(), Response::HTTP_BAD_REQUEST)
        );
    }
}
