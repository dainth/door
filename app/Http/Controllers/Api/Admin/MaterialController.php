<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MaterialRequest;
use App\Models\Material;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MaterialController extends Controller
{
    public function list(Request $request)
    {
        $name = $request->input('name');
        $materials = Material::getMaterials($name);

        return response()->json($materials, Response::HTTP_OK);
    }

    public function store(MaterialRequest $request)
    {
        $material = Material::create(['name' => $request->input('name')]);

        return response()->json($material, Response::HTTP_CREATED);
    }

    public function update($id, MaterialRequest $request)
    {
        $material = Material::findOrFail($id);
        $material->update(['name' => $request->input('name')]);

        return response()->json($material, Response::HTTP_OK);
    }

    public function destroy($id)
    {
        $material = Material::findOrFail($id);
        $material->delete();

        return response()->json('', Response::HTTP_NO_CONTENT);
    }
}
