<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function list(Request $request)
    {
        $name = $request->input('name');
        $status = $request->input('status');
        $categories = Category::getCategories($name, $status);

        return response()->json($categories, Response::HTTP_OK);
    }

    public function listParent()
    {
        $parentCategories = Category::getParentCategories();

        return response()->json($parentCategories, Response::HTTP_OK);
    }

    public function store(CategoryRequest $request)
    {
        $category = Category::create(array_merge($request->all(), ['slug' => Str::slug($request->input('name') . '-' . rand(0, 99))]));

        return response()->json($category, Response::HTTP_CREATED);
    }

    public function update($id, CategoryRequest $request)
    {
        $category = Category::findOrFail($id);
        $category->update(array_merge($request->all(), ['slug' => Str::slug($request->input('name') . '-' . rand(0, 99))]));

        return response()->json($category, Response::HTTP_OK);
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        if ($category->product->count()) {
            return response()->json('', Response::HTTP_BAD_REQUEST);
        }
        $category->delete();

        return response()->json('', Response::HTTP_NO_CONTENT);
    }

    public function upload(Request $request)
    {
        $image = $request->file('image');
        $image && $image = $this->uploadImage($image, CATEGORIES);

        return response()->json($image, Response::HTTP_OK);
    }
}
