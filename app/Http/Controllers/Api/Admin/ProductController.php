<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Color;
use App\Models\Material;
use App\Models\OptionProduct;
use App\Models\Product;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function list(Request $request)
    {
        $name = $request->input('name');
        $category_id = $request->input('category_id');
        $products = Product::getProducts($name, $category_id);

        return response()->json($products, Response::HTTP_OK);
    }

    public function listCategory(Request $request)
    {
        $name = $request->input('name');
        $categories = Category::getCategoriesInProduct($name);

        return response()->json($categories, Response::HTTP_OK);
    }

    public function listMaterial(Request $request)
    {
        $name = $request->input('name');
        $materials = Material::getMaterialsInProduct($name);

        return response()->json($materials, Response::HTTP_OK);
    }

    public function listColor(Request $request)
    {
        $name = $request->input('name');
        $colors = Color::getColorsInProduct($name);

        return response()->json($colors, Response::HTTP_OK);
    }

    public function detail($id)
    {
        $product = Product::with(['categories:id,name'])->findOrFail($id);
        $option_products = OptionProduct::with(['materials:id,name', 'colors:id,name'])->whereProductId($id)->get();
        $data = [
            'product' => $product,
            'option_products' => $option_products
        ];

        return response()->json($data, Response::HTTP_OK);
    }

    public function store(ProductRequest $request)
    {
        try {
            DB::beginTransaction();
            $product = Product::create(array_merge($request->all(), ['slug' => Str::slug($request->input('name') . '-' . rand(0, 99))]));
            if ($product) {
                foreach ($request->input('options') as $option) {
                    OptionProduct::create(array_merge($option, ['product_id' => $product->id]));
                }
            }
            DB::commit();
            return response()->json($product, Response::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update($id, ProductRequest $request)
    {
        $product = Product::findOrFail($id);
        $product->update(array_merge($request->all(), ['slug' => Str::slug($request->input('name') . '-' . rand(0, 99))]));
        $option_products = $request->input('options');
        foreach ($option_products as $item) {
            OptionProduct::findOrFail($item['id'])->update($item);
        }

        return response()->json($product, Response::HTTP_OK);
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        return response()->json('', Response::HTTP_NO_CONTENT);
    }

    public function upload(Request $request)
    {
        $image = $request->file('image');
        $image && $image = $this->uploadImage($image, PRODUCTS);

        return response()->json($image, Response::HTTP_OK);
    }
}
