<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function list(Request $request)
    {
        $name = $request->input('name');
        $phone = $request->input('phone');
        $email = $request->input('email');
        $status = $request->input('status');
        $users = User::getUsers($name, $phone, $email, $status);

        return response()->json($users, Response::HTTP_OK);
    }

    public function store(UserRequest $request)
    {
        $user = User::create(array_merge($request->all(), ['password' => \Hash::make($request->input('password'))]));

        return response()->json($user, Response::HTTP_CREATED);
    }

    public function update($id, UserRequest $request)
    {
        $user = User::findOrFail($id);
        if ($user->status === TWO) return response()->json(['message' => 'Tài khoản này không được sửa'], Response::HTTP_BAD_REQUEST);
        $user->update($request->all());

        return response()->json($user, Response::HTTP_OK);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user->status === TWO) return response()->json(['message' => 'Tài khoản này không được xóa'], Response::HTTP_BAD_REQUEST);
        $user->delete();

        return response()->json('', Response::HTTP_NO_CONTENT);
    }

    public function upload(Request $request)
    {
        $image = $request->file('image');
        if ($image) $image = $this->uploadImage($image, USERS);

        return response()->json($image, Response::HTTP_OK);
    }

    public function checkEmail(Request $request)
    {
        $id = $request->input('id');
        $email = $request->input('email');
        $check = User::where('email', $email)
            ->when($id, function ($qr) use ($id) {
                $qr->where('id', '!=', $id);
            })->first();
        if ($check) return response()->json(['message' => 'Email đã được sử dụng'], Response::HTTP_BAD_REQUEST);
        else return response()->json('', Response::HTTP_OK);
    }

    public function checkPassword(Request $request)
    {
        $password = $request->input('password');
        $user = User::findOrFail(\Auth::user()->id);
        if (Hash::check($password, $user->password)) return response()->json('', Response::HTTP_OK);
        else return response()->json(['message' => 'Mật khẩu không đúng'], Response::HTTP_BAD_REQUEST);
    }

    public function updatePassword(Request $request)
    {
        $password = $request->input('password');
        $user = User::findOrFail(\Auth::user()->id);
        $user->update(['password' => Hash::make($password)]);

        return response()->json('', Response::HTTP_OK);
    }
}
