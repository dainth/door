<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class ManageImageController extends Controller
{
    public function getImageInFolder(Request $request)
    {
        $name_folder = $request->input('name');
        if ($this->check($name_folder)) {
            $images = [];
            $files = \File::files('files/' . $name_folder);
            foreach ($files as $file) {
                $images[] = 'files/' . $name_folder . '/' . $file->getFilename();
            }
            return response()->json($images, 200);
        }
        return response()->json('', 400);
    }

    public function getImageInData(Request $request)
    {
        $name_path = $request->input('name');
        $images = [];
        $data = [];
        switch ($name_path) {
            case 'users' :
                $data = User::get('image');
                break;
            case 'categories':
                $data = Category::get('image');
                break;
            case 'products':
                $data = Product::with('optionProduct:product_id,image')->get(['id', 'image']);
                break;
        }
        foreach ($data as $item) {
            $item->image && $images[] = $item->image;
            if ($item->optionProduct) {
                foreach ($item->optionProduct as $optionProduct) {
                    $optionProduct->image && $images[] = $optionProduct->image;
                }
            }
        }
        return response()->json($images, 200);
    }

    public function check($name_folder)
    {
        $array_name_folder = [PRODUCTS, CATEGORIES, USERS];
        if (array_intersect([$name_folder], $array_name_folder)) return true;
        else return false;
    }

    public function destroy(Request $request)
    {
        $image = $request->input('image');
        if ($image && is_array($image)) {
            foreach ($image as $item) {
                ($item && $item['image']) && \File::delete($item['image']);
            }
        } else $image && \File::delete($image);

        return response()->json('', Response::HTTP_NO_CONTENT);
    }
}
