<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ColorRequest;
use App\Models\Color;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ColorController extends Controller
{
    public function list(Request $request)
    {
        $name = $request->input('name');
        $colors = Color::getColors($name);

        return response()->json($colors, Response::HTTP_OK);
    }

    public function store(ColorRequest $request)
    {
        $color = Color::create([
            'name' => $request->input('name'),
            'code' => $request->input('code')
        ]);

        return response()->json($color, Response::HTTP_CREATED);
    }

    public function update($id, ColorRequest $request)
    {
        $color = Color::findOrFail($id);

        $color->update([
            'name' => $request->input('name') ? $request->input('name') : $color->name,
            'code' => $request->input('code') ? $request->input('code') : $color->name,
        ]);

        return response()->json($color, Response::HTTP_OK);
    }

    public function destroy($id)
    {
        $color = Color::findOrFail($id);
        $color->delete();

        return response()->json('', Response::HTTP_NO_CONTENT);
    }
}
