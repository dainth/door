<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\LoginRequest;
use App\Models\PasswordReset;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use function Composer\Autoload\includeFile;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $email = $request->input('email');
        $message = $this->check($email);
        if ($message) {
            return response()->json(['message' => $message], Response::HTTP_BAD_REQUEST);
        } else {
            $user = User::where('email', $email)->where('status', '!=', INACTIVE)->first();
            if (!$user || !Hash::check($request->input('password'), $user->password)) {
                return response()->json(['message' => 'Thông tin tài khoản hoặc mật khẩu không đúng!!!'], Response::HTTP_BAD_REQUEST);
            }
            $user['remember_me'] = $request->input('remember_me', false);
            $token = $user->createToken($user->email)->plainTextToken;

            return response()->json($token, Response::HTTP_OK);
        }
    }

    public function me()
    {
        $user = User::findOrFail(Auth::id());

        return response()->json($user, Response::HTTP_OK);
    }

    public function logout()
    {
        $user = auth()->user();
        $user->tokens()->delete();
        return response()->json('', Response::HTTP_OK);
    }

    protected function credentials(Request $request)
    {
        return $request->only('email', 'password', 'password_confirmation', 'token');
    }

    public function broker()
    {
        return Password::broker();
    }

    protected function guard()
    {
        return Auth::guard();
    }

    public function forgotPassword(ForgotPasswordRequest $request)
    {
        $email = $request->input('email');
        $message = $this->check($email);
        if ($message) {
            return response()->json(['message' => $message], Response::HTTP_OK);
        } else {
            $password_reset = PasswordReset::whereEmail($email)->first();
            $token = strtoupper(\Str::random(7));
            if ($password_reset) $password_reset->update(['token' => $token]);
            else PasswordReset::create(['email' => $email, 'token' => $token]);
            \Mail::send('forgot-password', compact('token'), function ($message) use ($email) {
                $message->to($email, $email)->subject(__('[DOOR] - Lấy lại mật khẩu'));
                $message->from(config('app.mail_username'), config('app.mail_from_name'));
            });
            return response()->json(['message' => 'Mã xác thực đã được gửi đến email, vui lòng kiểm tra hộp thư'], Response::HTTP_OK);
        }
    }

    public function verifyPassword(Request $request)
    {
        $email = $request->input('email');
        $token = $request->input('token');
        $password = $request->input('password');
        $password_reset = PasswordReset::whereEmail($email)->whereToken($token)->first();
        if (!$password_reset) {
            return response()->json(['message' => 'Vui lòng nhập lại email hoặc mã xác thực'], Response::HTTP_OK);
        } else if (Carbon::now()->diffInMinutes($password_reset->updated_at) > FIFTEEN) {
            return response()->json(['message' => 'Mã xác thực đã hết hạn'], Response::HTTP_OK);
        }
        $password_reset->delete();
        User::whereEmail($email)->first()->update(['password' => Hash::make($password)]);
        return response()->json(['message' => 'Cập nhật mật khẩu thành công'], Response::HTTP_OK);
    }

    public function check($email)
    {
        $message = null;
        $check_deleted_user = User::withTrashed()->where('email', $email)->whereNotNull('deleted_at')->first();
        if ($check_deleted_user) $message = 'Tài khoản không còn tồn tại!!!';

        $check_email_user = User::where('email', $email)->first();
        if (!$check_email_user) $message = 'Thông tin tài khoản hoặc mật khẩu không đúng!!!';

        $check_status_user = User::where('email', $email)->where('status', INACTIVE)->first();
        if ($check_status_user) $message = 'Tài khoản đăng nhập không còn hoạt động!!!';

        return $message;
    }
}
