<?php

use Illuminate\Support\Facades\Route;

Route::fallback(function () {
    return view('index');
});
Route::get('/{any?}', 'HomeController@index')
    ->where('any', '^(?!(api|change|next|)).*$');
