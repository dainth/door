<?php

use Illuminate\Support\Facades\Route;

Broadcast::routes(['middleware' => ['auth:sanctum']]);

//auth
Route::group(['namespace' => 'Api\Auth'], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'AuthController@login');
        Route::post('forgot-password', 'AuthController@forgotPassword');
        Route::post('verify-password', 'AuthController@verifyPassword');
        Route::post('check-token', 'AuthController@checkToken');
        Route::middleware(['auth:sanctum'])->group(function () {
            Route::post('me', 'AuthController@me');
            Route::post('logout', 'AuthController@logout');
        });
    });
});

//admin
Route::middleware(['auth:sanctum'])->group(function () {
    Route::group(['namespace' => 'Api\Admin'], function () {
        Route::group(['prefix' => 'category'], function () {
            Route::get('list', 'CategoryController@list');
            Route::get('list-parent', 'CategoryController@listParent');
            Route::post('upload-image', 'CategoryController@upload');
            Route::post('store', 'CategoryController@store');
            Route::put('update/{id}', 'CategoryController@update');
            Route::delete('delete/{id}', 'CategoryController@destroy');
        });

        Route::group(['prefix' => 'color'], function () {
            Route::get('list', 'ColorController@list');
            Route::post('store', 'ColorController@store');
            Route::put('update/{id}', 'ColorController@update');
            Route::delete('delete/{id}', 'ColorController@destroy');
        });

        Route::group(['prefix' => 'material'], function () {
            Route::get('list', 'MaterialController@list');
            Route::post('store', 'MaterialController@store');
            Route::put('update/{id}', 'MaterialController@update');
            Route::delete('delete/{id}', 'MaterialController@destroy');
        });

        Route::group(['prefix' => 'product'], function () {
            Route::get('list', 'ProductController@list');
            Route::get('detail/{id}', 'ProductController@detail');
            Route::get('list-category', 'ProductController@listCategory');
            Route::get('list-material', 'ProductController@listMaterial');
            Route::get('list-color', 'ProductController@listColor');
            Route::post('upload-image', 'ProductController@upload');
            Route::post('store', 'ProductController@store');
            Route::put('update/{id}', 'ProductController@update');
            Route::delete('delete/{id}', 'ProductController@destroy');
        });

        Route::group(['prefix' => 'user'], function () {
            Route::get('list', 'UserController@list');
            Route::post('store', 'UserController@store');
            Route::put('update/{id}', 'UserController@update');
            Route::delete('delete/{id}', 'UserController@destroy');
            Route::post('upload-image', 'UserController@upload');
            Route::post('check-email', 'UserController@checkEmail');
            Route::post('check-password', 'UserController@checkPassword');
            Route::post('update-password', 'UserController@updatePassword');
        });

        Route::group(['prefix' => 'manage'], function () {
            Route::get('list-image-in-folder', 'ManageImageController@getImageInFolder');
            Route::get('list-image-in-data', 'ManageImageController@getImageInData');
            Route::post('delete', 'ManageImageController@destroy');
        });
    });
});
