import {ValidatorFn, AbstractControl, ValidationErrors} from '@angular/forms';

function isEmptyInputValue(value: any): boolean {
  return value == null || value.length === 0;
}

const EMAIL_REGEXP = /^(?=.{1,254}$)(?=.{1,64}@)[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+(\.[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+)*@[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?(\.[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?)*$/;

export class CommonValidator {
  validator: ValidatorFn;

  static isNumber(control: AbstractControl): ValidationErrors | null {
    const isValid = /^[1-9][0-9]*$/.test(control.value);
    if (isValid) {
      return null;
    } else {
      return {
        'isNumber': true
      };
    }
  }

  static isNumberFrom0(control: AbstractControl): ValidationErrors | null {
    const isValid = /^[0-9][0-9]*$/.test(control.value);
    if (isValid) {
      return null;
    } else {
      return {
        'isNumberFrom0': true
      };
    }
  }

  static isFloatFrom0(control: AbstractControl): ValidationErrors | null {
    const isValid = /^\d*(\.\d+)?$/.test(control.value);
    if (isValid) {
      return null;
    } else {
      return {
        'isFloatFrom0': true
      };
    }
  }

  static isNumberNullable(control: AbstractControl): ValidationErrors | null {
    const isValid = /^[1-9][0-9]*$/.test(control.value);
    if (isValid || isEmptyInputValue(control.value)) {
      return null;
    } else {
      return {
        'isNumberNullable': true
      };
    }
  }

  static isNumberFrom0Nullable(control: AbstractControl): ValidationErrors | null {
    const isValid = /^[0-9][0-9]*$/.test(control.value);
    if (isValid || isEmptyInputValue(control.value)) {
      return null;
    } else {
      return {
        'isNumberFrom0Nullable': true
      };
    }
  }

  static emailNullable(control: AbstractControl): ValidationErrors | null {
    if (isEmptyInputValue(control.value)) {
      return null;
    }
    return EMAIL_REGEXP.test(control.value) ? null : {'emailNullable': true};
  }

  static isValidPassword(control: AbstractControl): ValidationErrors | null {
    const isValid = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(control.value);
    if (isValid) {
      return null;
    } else {
      return {
        'isValidPassword': true
      };
    }
  }

  static isValidNumberAccumulated(control: AbstractControl): ValidationErrors | null {
    const isValid = /^[+-]?([0-9]*[.])?[0-9]+$/.test(control.value);
    if (isValid) {
      return null;
    } else {
      return {
        'isValidNumberAccumulated': true
      };
    }
  }
}
