export class User {
  id: number = null;
  name: string = '';
  email: string = '';
  status: number = 0;
  phone: number = null;
  gender: number = 2;
  image: string = '';
  birth: string = '';
  address: string = '';
  created_at: string = '';

  constructor(props) {
    if (props) {
      Object.keys(props).map(item => {
        if (item in this) {
          this[item] = props[item];
        }
      });
    }
  }
}
