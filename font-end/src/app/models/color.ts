export class Color {
  id: number = null;
  name:string = '';
  code:string= '';
  created_at:string = '';

  constructor(props?) {
    if (props) {
      Object.keys(props).map(key => {
        if (key in this) {
          this[key] = props[key]
        }
      });
    }
  }

}

