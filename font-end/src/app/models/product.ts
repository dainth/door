export class Product {
  id: number = null;
  category_id: number = null;
  categories: object = {id: null, name: null};
  name: string = '';
  slug: string = '';
  length: string = '';
  width: string = '';
  height: string = '';
  weight: string = '';
  price: number = null;
  image: string = '';
  content: string = '';
  created_at: string = '';

  constructor(props?) {
    if (props) {
      Object.keys(props).map(key => {
        if (key in this) {
          this[key] = props[key]
        }
      });
    }
  }

}

