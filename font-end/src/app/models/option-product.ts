export class OptionProduct {
  id: number = null;
  product_id: number = null;
  color_id: number = null;
  material_id: number = null;
  price: number = null;
  materials: object = {id: null, name: null};
  colors: object = {id: null, name: null};
  image: string = '';

  constructor(props?) {
    if (props) {
      Object.keys(props).map(key => {
        if (key in this) {
          this[key] = props[key]
        }
      });
    }
  }

}

