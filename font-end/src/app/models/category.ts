export class Category {
  id: number = null;
  category_id: number = null;
  name: string = '';
  slug: string = '';
  image: string = '';
  icon: string = '';
  status: number = 1;
  created_at: string = '';

  constructor(props?) {
    if (props) {
      Object.keys(props).map(key => {
        if (key in this) {
          this[key] = props[key]
        }
      });
    }
  }

}

