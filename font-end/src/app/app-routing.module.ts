import {Injectable} from '@angular/core';
import {CanActivate, Router, Routes} from '@angular/router';
import {AuthService} from './services/auth.service';
import {LayoutAuthComponent} from "./components/layout/layout-auth/layout-auth.component";
import {LayoutAdminComponent} from "./components/layout/layout-admin/layout-admin.component";
import {LayoutClientComponent} from "./components/layout/layout-client/layout-client.component";

@Injectable()
export class CanActiveAdmin implements CanActivate {
  constructor(private router: Router, private authService: AuthService) {
  }

  canActivate() {
    if (this.authService.isTokenExpired()) {
      return true;
    }
    this.router.navigate(['/auth/login']);
    this.authService.removeLocalStorage();
    return false;
  }
}

export const AppRoutes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./components/auth/auth.module').then(m => m.AuthModule),
    component: LayoutAuthComponent,
    data: {title: 'DOOR | Đăng nhập - Đăng ký'}
  },
  {
    path: 'admin',
    canActivate: [CanActiveAdmin],
    loadChildren: () => import('./components/admin/admin.module').then(m => m.AdminModule),
    component: LayoutAdminComponent,
    data: {title: 'DOOR | Quản lý'}
  },
  {
    path: 'trang-chu',
    loadChildren: () => import('./components/client/client.module').then(m => m.ClientModule),
    component: LayoutClientComponent,
    data: {title: 'DOOR | Mua sản phẩm thuận tiện nhất'}
  },
  {
    path: '**',
    redirectTo: '',
    component: LayoutClientComponent,
    pathMatch: 'full'
  },
];
