import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';

export const TOKEN = 'token';
export const USER = 'user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrlApi = null;

  constructor(
    private http: HttpClient
  ) {
    this.baseUrlApi = environment.baseUrlApi;
  }

  headerBody() {
    const headers = new HttpHeaders();
    return headers.append('Authorization', 'Bearer ' + this.getTokenLocalStorage());
  }

  login(body: any) {
    return this.http.post(this.baseUrlApi + 'auth/login', body);
  }

  logout() {
    const headers = this.headerBody();
    return this.http.post(this.baseUrlApi + 'auth/logout', null, {headers});
  }

  forgotPassword(body) {
    return this.http.post(this.baseUrlApi + 'auth/forgot-password', body);
  }

  verifyPassword(body) {
    return this.http.post(this.baseUrlApi + 'auth/verify-password', body);
  }

  checkToken(body) {
    return this.http.post(this.baseUrlApi + 'auth/check-token', body);
  }

  me() {
    const headers = this.headerBody();
    return this.http.post(this.baseUrlApi + 'auth/me', null, {headers});
  }

  setTokenLocalStorage(token) {
    return localStorage.setItem(TOKEN, JSON.stringify(token));
  }

  setUserLocalStorage(user) {
    return localStorage.setItem(USER, JSON.stringify(user));
  }

  getTokenLocalStorage() {
    return JSON.parse(localStorage.getItem(TOKEN));
  }

  getUserLocalStorage() {
    return JSON.parse(localStorage.getItem(USER));
  }

  removeLocalStorage() {
    return [localStorage.removeItem(TOKEN), localStorage.removeItem(USER)];
  }


  isTokenExpired(): boolean {
    const token = this.getTokenLocalStorage();
    if (token) {
      return true;
    } else {
      return false;
    }
  }

  isUserExpired(): boolean {
    const user = this.getUserLocalStorage();
    if (user) {
      return true;
    } else {
      return false;
    }
  }
}
