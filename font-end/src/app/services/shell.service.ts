import {Injectable} from '@angular/core';
import {Route, Routes} from '@angular/router';
import {LayoutAuthComponent} from '../components/layout/layout-auth/layout-auth.component';
import {LayoutAdminComponent} from '../components/layout/layout-admin/layout-admin.component';

@Injectable({
  providedIn: 'root'
})
export class ShellService {
  static childRoutesAuth(routes: Routes): Route {
    return {
      path: '',
      component: LayoutAuthComponent,
      children: routes,
      data: {reuse: true}
    };
  }

  static childRouteAdmin(routes: Routes): Route {
    return {
      path: '',
      component: LayoutAdminComponent,
      children: routes,
      data: {reuse: true}
    };
  }
}
