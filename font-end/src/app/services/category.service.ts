import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {TOKEN} from "./auth.service";

@Injectable({
  providedIn: 'root'
})

export class CategoryService {

  baseUrlApi = null;

  constructor(
    private http: HttpClient
  ) {
    this.baseUrlApi = environment.baseUrlApi;
  }

  headerBody() {
    const headers = new HttpHeaders();
    return headers.append('Authorization', 'Bearer ' + this.getTokenLocalStorage());
  }

  list(body = null) {
    const headers = this.headerBody();
    return this.http.get(this.baseUrlApi + 'category/list', {headers, params: body});
  }

  uploadImage(body) {
    const headers = this.headerBody();
    let formData = new FormData();
    formData = this.convertModelToFormData(body);
    return this.http.post(this.baseUrlApi + 'category/upload-image', formData, {headers});
  }

  add(body) {
    const headers = this.headerBody();
    return this.http.post(this.baseUrlApi + 'category/store', body, {headers});
  }

  update(id, body) {
    const headers = this.headerBody();
    return this.http.put(this.baseUrlApi + 'category/update/' + id, body, {headers});
  }

  delete(id) {
    const headers = this.headerBody();
    return this.http.delete(this.baseUrlApi + 'category/delete/' + id, {headers});
  }

  getTokenLocalStorage() {
    return JSON.parse(localStorage.getItem(TOKEN));
  }

  convertModelToFormData(val, formData = new FormData(), namespace = '') {
    if ((typeof val !== 'undefined') && (val !== null)) {
      if (val instanceof Date) {
        formData.append(namespace, val.toISOString());
      } else if (val instanceof Array) {
        for (let i = 0; i < val.length; i++) {
          this.convertModelToFormData(val[i], formData, namespace + '[' + i + ']');
        }
      } else if (typeof val === 'object' && !(val instanceof File)) {
        if (val instanceof FileList) {
          for (let i = 0; i < val.length; i++) {
            formData.append(namespace + '[]', val[i]);
          }
        } else {
          for (const propertyName in val) {
            if (val.hasOwnProperty(propertyName)) {
              this.convertModelToFormData(val[propertyName], formData, namespace ? namespace + '[' + propertyName + ']' : propertyName);
            }
          }
        }
      } else if (val instanceof File) {
        formData.append(namespace, val);
      } else {
        formData.append(namespace, val.toString());
      }
    }
    return formData;
  }
}
