import {Injectable, Injector} from '@angular/core';
import {HttpEvent, HttpHandler, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {finalize, tap} from "rxjs/operators";
import {Router} from "@angular/router";
import {AuthService} from "./auth.service";

export const TOKEN = 'token';
export const USER = 'user';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  private authService: AuthService;

  constructor(
    private router: Router,
    private injector: Injector) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.authService = this.injector.get(AuthService);
    request = request.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.authService.getTokenLocalStorage(),
        Accept: 'application/json',
      }
    });

    return next.handle(request).pipe(
      tap(
        event => {
        },
        error => {
          if (error.status === 401 || JSON.parse(localStorage.getItem(TOKEN)) === null || JSON.parse(localStorage.getItem(USER)) === null) {
            this.router.navigate(['/auth/login']);
            this.authService.removeLocalStorage();
          }
        }
      ),
      finalize(() => {

      })
    );
  }
}
