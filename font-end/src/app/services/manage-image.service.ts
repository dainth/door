import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {TOKEN} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class ManageImageService {

  baseUrlApi = null;

  constructor(
    private http: HttpClient
  ) {
    this.baseUrlApi = environment.baseUrlApi;
  }

  headerBody() {
    const headers = new HttpHeaders();
    return headers.append('Authorization', 'Bearer ' + this.getTokenLocalStorage());
  }

  listImageInFolder(body = null) {
    const headers = this.headerBody();
    return this.http.get(this.baseUrlApi + 'manage/list-image-in-folder', {headers, params: body});
  }

  listImageInData(body = null) {
    const headers = this.headerBody();
    return this.http.get(this.baseUrlApi + 'manage/list-image-in-data', {headers, params: body});
  }

  delete(body) {
    const headers = this.headerBody();
    return this.http.post(this.baseUrlApi + 'manage/delete', body, {headers});
  }

  getTokenLocalStorage() {
    return JSON.parse(localStorage.getItem(TOKEN));
  }
}
