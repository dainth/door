import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {TOKEN} from "./auth.service";

@Injectable({
  providedIn: 'root'
})

export class MaterialService {

  baseUrlApi = null;

  constructor(
    private http: HttpClient
  ) {
    this.baseUrlApi = environment.baseUrlApi;
  }

  headerBody() {
    const headers = new HttpHeaders();
    return headers.append('Authorization', 'Bearer ' + this.getTokenLocalStorage());
  }

  list(body = null) {
    const headers = this.headerBody();
    return this.http.get(this.baseUrlApi + 'material/list', {headers, params: body});
  }

  add(body) {
    const headers = this.headerBody();
    return this.http.post(this.baseUrlApi + 'material/store', body, {headers});
  }

  update(id, body) {
    const headers = this.headerBody();
    return this.http.put(this.baseUrlApi + 'material/update/' + id, body, {headers});
  }

  delete(id) {
    const headers = this.headerBody();
    return this.http.delete(this.baseUrlApi + 'material/delete/' + id, {headers});
  }

  getTokenLocalStorage() {
    return JSON.parse(localStorage.getItem(TOKEN));
  }
}
