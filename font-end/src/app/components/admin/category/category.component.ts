import {Component, OnInit} from '@angular/core';
import {CategoryService} from "../../../services/category.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {Category} from "../../../models/category";
import swal from 'sweetalert2';
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  imageDefault = environment.imageDefault;
  categoryForm: FormGroup;
  pagination = {
    current_page: null,
    last_page: null
  };
  searchForm: FormGroup;
  categories: Array<Category> = [];
  submitted = false;
  showAddEdit: boolean;
  addOrEdit: boolean;
  showSearch: boolean;
  loading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private categoryService: CategoryService,
    private toast: ToastrService,
  ) {
  }

  buildForm(data?) {
    this.categoryForm = this.formBuilder.group({
      id: [data ? data.id : null],
      category_id: [data ? data.category_id : null],
      name: [data ? data.name : null, [Validators.required, Validators.maxLength(255)]],
      slug: [data ? data.slug : null],
      icon: [data ? data.icon : null, [Validators.maxLength(255)]],
      status: [data ? data.status : 1],
      image: [data ? data.image : null]
    });
  }

  buildFormSearch() {
    this.searchForm = this.formBuilder.group({
      name: [''],
      status: [''],
      page: [1],
    })
  }

  get categoryFormControl() {
    return this.categoryForm.controls;
  }

  ngOnInit(): void {
    this.buildFormSearch();
    this.list();
  }

  list() {
    this.loading = true;
    this.categoryService.list(this.searchForm.value).subscribe((res: any) => {
      this.categories = res.data.map(val => {
        return new Category(val);
      });
      this.pagination.current_page = res.current_page;
      this.pagination.last_page = res.last_page;
      this.loading = false;
    }, (error) => {
      this.toast.error(error, 'Lỗi!!!');
    });
    this.close();
  }

  addAndEdit(item?) {
    this.showAddEdit = true;
    this.addOrEdit = !!item;
    this.buildForm(item);
  }

  search() {
    this.showSearch = true;
  }

  save() {
    this.submitted = true;
    console.log(this.categoryForm);
    if (this.categoryForm.valid) {
      const apiCall = this.addOrEdit ? this.categoryService.update(this.categoryForm.value.id, this.categoryForm.value) : this.categoryService.add(this.categoryForm.value);
      apiCall.subscribe((res: any) => {
        const title = this.addOrEdit ? 'Sửa danh mục thành công' : 'Thêm danh mục thành công';
        this.toast.success(title, 'Thành công');
        this.list();
        this.close();
      }, ((error) => {
        if (error.status === 400) {
          Object.keys(error.error).map(key => {
            this.toast.warning(error.error[key], 'Cảnh báo!!!');
          });
        } else {
          this.toast.error('Có lỗi xảy ra', 'Lỗi!!!');
        }
      }))
    }
  }

  delete(id: number) {
    swal.fire({
      title: 'Bạn chắc chắn muốn xóa không?',
      text: null,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Xóa',
      cancelButtonText: 'Đóng'
    }).then((result) => {
      if (result.value) {
        this.categoryService.delete(id).subscribe((res: any) => {
          swal.fire(
            'Xóa thành công',
            '',
            'success'
          );
          this.list();
        }, (error) => {
          if (error.status === 400) {
            swal.fire(
              'Lỗi',
              'Danh mục đang có sản phẩm, không thể xóa',
              'error'
            );
          }
        })
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal.fire(
          'Đóng',
          '',
          'error'
        )
      }
    })
  }

  page(event) {
    this.searchForm.get('page').setValue(event);
    this.list();
  }

  load() {
    this.buildFormSearch();
    this.list();
    this.close();
  }

  upImage(event) {
    const file = event.dataTransfer ? event.dataTransfer.files[0] : event.target.files[0];
    if (file !== undefined) {
      const pattern = /image-*/;
      if (event.target.files[0] && event.target.files[0].size >= 5242880) this.toast.warning("Ảnh bạn phải nhỏ hơn 5MB", 'Cảnh báo');
      else if (!file.type.match(pattern)) this.toast.warning("Định dạng ảnh không hợp lệ", 'Cảnh báo');
      else {
        this.categoryService.uploadImage({image: file}).subscribe((res: any) => {
          this.toast.success('Thêm ảnh thành công', 'Thành công');

          this.categoryForm.get('image').setValue(res);
        }, (error) => {
          this.toast.error(error, 'Lỗi!!!');
        });
      }
    }
  }

  close() {
    this.categoryForm = undefined;
    this.submitted = false;
    this.showAddEdit = false;
    this.showSearch = false;
  }

  // push(obj) {
  //   // return {...this.categories, ...obj}
  //   this.categories.splice(0, 0, obj);
  // }
}
