import {Component, OnInit} from '@angular/core';
import {ManageImageService} from "../../../services/manage-image.service";
import {ToastrService} from "ngx-toastr";
import {FormBuilder, FormGroup} from "@angular/forms";
import swal from "sweetalert2";

@Component({
  selector: 'app-manage-image',
  templateUrl: './manage-image.component.html',
  styleUrls: ['./manage-image.component.scss']
})
export class ManageImageComponent implements OnInit {

  loading = true;
  images: Array<any> = [{image: null, deleted: true, selected: false}];
  searchForm: FormGroup;
  multiOrOne = false;

  buildFormSearch() {
    this.searchForm = this.formBuilder.group({
      name: ['products'],
    })
  }

  constructor(
    private formBuilder: FormBuilder,
    private manageImageService: ManageImageService,
    private toast: ToastrService,
  ) {
    this.buildFormSearch();
  }

  ngOnInit(): void {
    this.listImage();
  }

  load() {
    this.buildFormSearch();
    this.listImage();
  }

  search() {
    this.listImage();
  }

  listImage() {
    this.manageImageService.listImageInFolder(this.searchForm.value).subscribe((imageInFolder: Array<string>) => {
      this.manageImageService.listImageInData(this.searchForm.value).subscribe((imageInData: Array<string>) => {
        this.images = imageInFolder.map((i: string) => {
          return {
            image: i,
            selected: false,
            deleted: !imageInData.find(j => j === i)
          };
        });
        this.loading = false;
      }, (error) => {
        this.toast.error(error, 'Lỗi!!!');
      });
    }, (error) => {
      this.toast.error(error, 'Lỗi!!!');
    });
  }

  clickMultiSelect() {
    this.multiOrOne = !this.multiOrOne;
  }

  multiSelected(item, index) {
    this.images[index].selected ? this.images[index].selected = false : this.images[index].selected = true;
  }

  delete(image?) {
    swal.fire({
      title: 'Bạn chắc chắn muốn xóa không?',
      text: null,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Xóa',
      cancelButtonText: 'Đóng'
    }).then((result) => {
      if (result.value) {
        let data: object = {};
        this.multiOrOne ? data = {image: this.images.filter(i => i.selected)} : data = {image: image};
        this.manageImageService.delete(data).subscribe(() => {
          swal.fire(
            'Xóa thành công',
            '',
            'success'
          );
          this.listImage();
          this.multiOrOne = false;
        }, (error) => {
          swal.fire(
            'Lỗi',
            error,
            'error'
          );
        })
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal.fire(
          'Đóng',
          '',
          'error'
        )
      }
    })
  }

}
