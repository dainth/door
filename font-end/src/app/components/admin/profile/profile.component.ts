import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../services/auth.service";
import {UserService} from "../../../services/user.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  imageDefault = environment.imageDefault;
  user: any;
  userForm: FormGroup;
  submitted: boolean;
  showEdit: boolean;

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private toast: ToastrService,
    private router: Router
  ) {
  }

  buildForm() {
    const numbers = /^[+-]?((\.\d+)|(\d+(\.\d+)?))$/;
    this.userForm = this.formBuilder.group({
      id: [this.user ? this.user.id : null],
      profile: [true],
      name: [this.user ? this.user.name : null, [Validators.required, Validators.maxLength(255)]],
      email: [this.user ? this.user.email : null, [Validators.required, Validators.maxLength(255)]],
      gender: [this.user ? this.user.gender : null],
      birth: [this.user ? this.user.birth : null],
      phone: [this.user ? this.user.phone : null, [Validators.required, Validators.maxLength(255), Validators.pattern(numbers), Validators.min(0)]],
      address: [this.user ? this.user.address : null, [Validators.required, Validators.maxLength(255)]],
      image: [this.user ? this.user.image : null],
    });
  }

  get userFormControl() {
    return this.userForm.controls;
  }

  ngOnInit(): void {
    this.getUser();
  }

  getUser() {
    this.user = this.authService.getUserLocalStorage();
    this.buildForm();
    if (!this.user) {
      this.router.navigate(['/auth/login']);
    }
  }

  edit() {
    this.showEdit = true;
  }

  save() {
    this.submitted = true;
    if (this.userForm.valid) {
      this.userService.update(this.userForm.value.id, this.userForm.value).subscribe((res: any) => {
        this.toast.success('Sửa thông tin thành công', 'Thành công');
        this.authService.me().subscribe((res: any) => {
          this.authService.setUserLocalStorage(res);
          this.getUser();
        });
        this.close();
      }, (error) => {
        if (error.status === 400) {
          Object.keys(error.error).map(key => {
            this.toast.warning(error.error[key], 'Cảnh báo!!!');
          });
        } else {
          this.toast.error('Có lỗi xảy ra', 'Lỗi!!!');
        }
      })
    }
  }

  upImage(event) {
    const file = event.dataTransfer ? event.dataTransfer.files[0] : event.target.files[0];
    if (file !== undefined) {
      const pattern = /image-*/;
      if (event.target.files[0] && event.target.files[0].size >= 5242880) this.toast.warning("Ảnh bạn phải nhỏ hơn 5MB", 'Cảnh báo');
      else if (!file.type.match(pattern)) this.toast.warning("Định dạng ảnh không hợp lệ", 'Cảnh báo');
      else {
        this.userService.uploadImage({image: file}).subscribe((res: any) => {
          this.toast.success('Thêm ảnh thành công', 'Thành công');
          this.userForm.get('image').setValue(res);
        }, (error) => {
          this.toast.error(error, 'Lỗi!!!');
        });
      }
    }
  }

  close() {
    this.submitted = false;
    this.showEdit = false
  }

}
