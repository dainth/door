import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProductService} from "../../../../../services/product.service";
import {ToastrService} from "ngx-toastr";
import {ActivatedRoute, Router} from "@angular/router";
import numeral from 'numeral';
import {environment} from "../../../../../../environments/environment";
import {debounce} from 'lodash';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-add-or-edit-product',
  templateUrl: './add-or-edit-product.component.html',
  styleUrls: ['./add-or-edit-product.component.scss']
})
export class AddOrEditProductComponent implements OnInit {

  productForm: FormGroup;
  id: number;
  categories: Array<any> = [];
  materials: Array<any> = [];
  colors: Array<any> = [];
  loading: boolean;
  imageDefault = environment.imageDefault;
  showDropDownCategory: boolean;
  showDropdownMaterial: boolean;
  submitted: boolean;
  public Editor = ClassicEditor;

  constructor(
    private formBuilder: FormBuilder,
    private productService: ProductService,
    public route: ActivatedRoute,
    private toast: ToastrService,
    private router: Router
  ) {
    this.id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
  }

  buildForm(product?, optionProducts?) {
    this.productForm = this.formBuilder.group({
      id: [product ? product.id : null],
      name: [product ? product.name : null, [Validators.required, Validators.maxLength(255)]],
      category_id: [product ? product.category_id : null, [Validators.required]],
      category_name: [product ? this.categories.find((i: any) => i.id === product.category_id) : null],
      image: [product ? product.image : null],
      length: [product ? product.length : null],
      width: [product ? product.width : null],
      height: [product ? product.height : null],
      weight: [product ? product.weight : null],
      content: [product ? product.content : null, [Validators.required]],
      options: this.formBuilder.array([])
    });
    if (optionProducts && optionProducts.length) {
      optionProducts.map(option => {
        this.addOption(option)
      })
    } else {
      this.addOption()
    }
  }

  get productFormControl() {
    return this.productForm.controls;
  }

  listCategory(body?) {
    this.productService.listCategory(body).subscribe((res: any) => {
      this.categories = res;
    }, (error) => {
      this.toast.error(error, 'Lỗi!!!');
    });
  }

  listMaterial(body?) {
    this.productService.listMaterial(body).subscribe((res: any) => {
      this.materials = res;
    }, (error) => {
      this.toast.error(error, 'Lỗi!!!');
    });
  }

  listColor(body?) {
    this.productService.listColor(body).subscribe((res: any) => {
      this.colors = res;
    }, (error) => {
      this.toast.error(error, 'Lỗi!!!');
    });
  }

  detail() {
    if (isNaN(this.id)) {
      this.buildForm()
    } else {
      this.productService.detail(this.id).subscribe((res: any) => {
        this.buildForm(res.product, res.option_products);
      }, (error) => {
        this.toast.error(error, 'Lỗi!!!');
      });
    }
  }

  ngOnInit(): void {
    this.listCategory();
    this.listMaterial();
    this.listColor();
    this.detail();
    this.buildForm();
  }

  save() {
    this.submitted = true;
    if (this.productForm.valid) {
      const apiCall = this.id ? this.productService.update(this.productForm.value.id, this.productForm.value) : this.productService.add(this.productForm.value);
      apiCall.subscribe(() => {
        const title = this.id ? 'Sửa sản phẩm thành công' : 'Thêm sản phẩm thành công';
        this.toast.success(title, 'Thành công');
        this.router.navigate(['admin/product']);
      }, ((error) => {
        if (error.status === 400) {
          Object.keys(error.error).map(key => {
            this.toast.warning(error.error[key], 'Cảnh báo!!!');
          });
        } else {
          this.toast.error('Có lỗi xảy ra', 'Lỗi!!!');
        }
      }))
    }
  }

  getOptionGroup(index, type?): FormGroup {
    type = this.productForm.get('options') as FormArray;
    return type.at(index) as FormGroup;
  }

  formatNumber(event, index) {
    let numberFormat = numeral(event.target.value).format('0,0');
    let number = numeral(numberFormat).value();
    this.getOptionGroup(index, 'priceFormat').get('priceFormat').setValue(numberFormat);
    this.getOptionGroup(index, 'price').get('price').setValue(number);
  }


  addOption(data?) {
    const add = this.productForm.controls.options as FormArray;
    const numbers = /^[+-]?((\.\d+)|(\d+(\.\d+)?))$/;
    add.push(this.formBuilder.group({
      id: [data ? data.id : null],
      material_id: [data ? data.material_id : null, [Validators.required]],
      color_id: [data ? data.color_id : null, [Validators.required]],
      image: [data ? data.image : null],
      price: [data ? data.price : null, [Validators.required, Validators.maxLength(255), Validators.pattern(numbers), Validators.min(0)]],
      priceFormat: [data ? (data.price ? numeral(data.price).format('0,0') : null) : null],
    }));
  }

  deleteOption(index) {
    const options = this.productForm.get('options') as FormArray;
    options.removeAt(index);
  }

  showDropdownMenuCategory() {
    this.showDropDownCategory = !this.showDropDownCategory;
  }

  showDropdownMenuMaterial(index?) {
    this.showDropdownMaterial = !this.showDropdownMaterial;
  }

  selectCategory(category) {
    this.productForm.get('category_id').setValue(category.id);
    this.productForm.get('category_name').setValue(category);
    this.showDropDownCategory = false;
  }

  selectMaterial(material) {
    this.productForm.get('material_id').setValue(material.id);
    this.productForm.get('material_name').setValue(material);
    this.showDropdownMaterial = false;
  }

  search(type?, event?) {
    if (type === 'searchCategory') {
      if (event) {
        const filterList = debounce(() => {
          this.listCategory({name: event.target.value})
        }, 300);
        filterList();
      }
    } else if (type === 'searchMaterial') {
      if (event) {
        const filterList = debounce(() => {
          this.listMaterial({name: event.target.value})
        }, 300);
        filterList();
      }
    }
  }

  upImage(event, index?, isMutilFile = false) {
    const file = event.dataTransfer ? event.dataTransfer.files[0] : event.target.files[0];
    if (file !== undefined) {
      const pattern = /image-*/;
      if (event.target.files[0] && event.target.files[0].size >= 5242880) this.toast.warning("Ảnh bạn phải nhỏ hơn 5MB", 'Cảnh báo');
      else if (!file.type.match(pattern)) this.toast.warning("Định dạng ảnh không hợp lệ", 'Cảnh báo');
      else {
        this.productService.uploadImage({image: file}).subscribe((res: any) => {
          this.toast.success('Thêm ảnh thành công', 'Thành công');
          if (isMutilFile) {
            this.getOptionGroup(index, 'image').get('image').setValue(res);
          } else {
            this.productForm.get('image').setValue(res);
          }
        }, (error) => {
          this.toast.error(error, 'Lỗi!!!');
        });
      }
    }
  }
}
