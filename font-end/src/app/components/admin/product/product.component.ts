import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {ProductService} from "../../../services/product.service";
import swal from "sweetalert2";
import {Router} from "@angular/router";
import {Category} from "../../../models/category";
import {Material} from "../../../models/material";
import {Product} from "../../../models/product";
import {OptionProduct} from "../../../models/option-product";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  pagination = {
    current_page: null,
    last_page: null
  };
  searchForm: FormGroup;
  products: Array<Product> = [];
  product: Product;
  optionProducts: Array<OptionProduct> = [];
  categories: Array<Category> = [];
  materials: Array<Material> = [];
  showSearch: boolean;
  showDetail: boolean;
  loading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private toast: ToastrService,
    private router: Router
  ) {
  }

  buildFormSearch() {
    this.searchForm = this.formBuilder.group({
      name: [''],
      category_id: [''],
      page: [1],
    })
  }

  ngOnInit(): void {
    this.buildFormSearch();
    this.list();
    this.listCategory();
    this.listMaterial();
  }

  list() {
    this.loading = true;
    this.productService.list(this.searchForm.value).subscribe((res: any) => {
      this.products = res.data.map(val => {
        return new Product(val);
      });
      this.pagination.current_page = res.current_page;
      this.pagination.last_page = res.last_page;
      this.loading = false;
    }, (error) => {
      this.toast.error(error, 'Lỗi!!!');
    });
    this.close();
  }

  listCategory(body?) {
    this.productService.listCategory(body).subscribe((res: any) => {
      this.categories = res.map(val => {
        return new Category(val);
      });
    }, (error) => {
      this.toast.error(error, 'Lỗi!!!');
    });
  }

  listMaterial(body?) {
    this.productService.listMaterial(body).subscribe((res: any) => {
      this.materials = res.map(val => {
        return new Material(val);
      });
    }, (error) => {
      this.toast.error(error, 'Lỗi!!!');
    });
  }

  addAndEdit(id?) {
    if (id) this.router.navigate(['/admin/product/edit/', id]);
    else this.router.navigate(['/admin/product/add']);
  }

  search() {
    this.showSearch = true;
  }

  detail(id) {
    this.showDetail = true;
    this.productService.detail(id).subscribe((res: any) => {
      this.product = new Product(res.product);
      this.optionProducts = res.option_products.map(val => {
        return new OptionProduct(val);
      });
    }, (error) => {
      this.toast.error(error, 'Lỗi!!!');
    });
  }

  delete(id: number) {
    swal.fire({
      title: 'Bạn chắc chắn muốn xóa không?',
      text: null,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Xóa',
      cancelButtonText: 'Đóng'
    }).then((result) => {
      if (result.value) {
        this.productService.delete(id).subscribe((res: any) => {
          swal.fire(
            'Xóa thành công',
            '',
            'success'
          );
          this.list();
        }, (error) => {
          swal.fire(
            'Lỗi',
            error,
            'error'
          );
        })
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal.fire(
          'Đóng',
          '',
          'error'
        )
      }
    })
  }

  page(event) {
    this.searchForm.value.page = event;
    this.list();
  }

  load() {
    this.buildFormSearch();
    this.list();
    this.close();
  }

  close() {
    this.showSearch = false;
    this.showDetail = false;
    this.product = undefined;
    this.optionProducts = [];
  }
}
