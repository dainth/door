import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Color} from "../../../models/color";
import {ToastrService} from "ngx-toastr";
import {ColorService} from "../../../services/color.service";
import swal from "sweetalert2";

@Component({
  selector: 'app-color',
  templateUrl: './color.component.html',
  styleUrls: ['./color.component.scss']
})
export class ColorComponent implements OnInit {

  colorForm: FormGroup;
  pagination = {
    current_page: null,
    last_page: null
  };
  searchForm: FormGroup;
  colors: Array<Color> = [];
  submitted = false;
  showAddEdit: boolean;
  addOrEdit: boolean;
  showSearch: boolean;
  loading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private colorService: ColorService,
    private toast: ToastrService,
  ) {
  }

  buildForm(data?) {
    this.colorForm = this.formBuilder.group({
      id: [data ? data.id : null],
      name: [data ? data.name : null, [Validators.required, Validators.maxLength(255)]],
      code: [data ? data.code : null, [Validators.required, Validators.maxLength(255)]]
    });
  }

  buildFormSearch() {
    this.searchForm = this.formBuilder.group({
      name: [''],
      page: [1],
    })
  }

  get colorFormControl() {
    return this.colorForm.controls;
  }

  ngOnInit(): void {
    this.buildFormSearch();
    this.list();
  }

  list() {
    this.loading = true;
    this.colorService.list(this.searchForm.value).subscribe((res: any) => {
      this.colors = res.data.map(val => {
        return new Color(val);
      });
      this.pagination.current_page = res.current_page;
      this.pagination.last_page = res.last_page;
      this.loading = false;
    }, (error) => {
      this.toast.error(error, 'Lỗi!!!');
    });
    this.close();
  }

  addAndEdit(item?) {
    this.showAddEdit = true;
    this.addOrEdit = !!item;
    this.buildForm(item);
  }

  search() {
    this.showSearch = true;
  }

  save() {
    this.submitted = true;
    if (this.colorForm.valid) {
      const apiCall = this.addOrEdit ? this.colorService.update(this.colorForm.value.id, this.colorForm.value) : this.colorService.add(this.colorForm.value);
      apiCall.subscribe((res: any) => {
        const title = this.addOrEdit ? 'Sửa màu sắc thành công' : 'Thêm màu sắc thành công';
        this.toast.success(title, 'Thành công');
        this.list();
        this.close();
      }, ((error) => {
        if (error.status === 400) {
          Object.keys(error.error).map(key => {
            this.toast.warning(error.error[key], 'Cảnh báo!!!');
          });
        } else {
          this.toast.error('Có lỗi xảy ra', 'Lỗi!!!');
        }
      }))
    }
  }

  delete(id: number) {
    swal.fire({
      title: 'Bạn chắc chắn muốn xóa không?',
      text: null,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Xóa',
      cancelButtonText: 'Đóng'
    }).then((result) => {
      if (result.value) {
        this.colorService.delete(id).subscribe((res: any) => {
          swal.fire(
            'Xóa thành công',
            '',
            'success'
          );
          this.list();
        }, (error) => {
          swal.fire(
            'Lỗi',
            error,
            'error'
          );
        })
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal.fire(
          'Đóng',
          '',
          'error'
        )
      }
    })
  }

  page(event) {
    this.searchForm.get('page').setValue(event);
    this.list();
  }

  load() {
    this.buildFormSearch();
    this.list();
    this.close();
  }

  close() {
    this.colorForm = undefined;
    this.submitted = false;
    this.showAddEdit = false;
    this.showSearch = false;
  }

}
