import {Routes} from '@angular/router';
import {CategoryComponent} from "./category/category.component";
import {ColorComponent} from "./color/color.component";
import {ProfileComponent} from "./profile/profile.component";
import {MaterialComponent} from "./material/material.component";
import {ProductComponent} from "./product/product.component";
import {UserComponent} from "./user/user.component";
import {AddOrEditProductComponent} from "./product/components/add-or-edit-product/add-or-edit-product.component";
import {ManageImageComponent} from "./manage-image/manage-image.component";

export const AdminRoutes: Routes = [
  {
    path: 'profile',
    component: ProfileComponent,
    data: {title: 'DOOR | Thông tin tài khoản'},
  },
  {
    path: 'category',
    component: CategoryComponent,
    data: {title: 'DOOR | Quản lý danh mục'},
  },
  {
    path: 'color',
    component: ColorComponent,
    data: {title: 'DOOR | Quản lý bảng màu'},
  },
  {
    path: 'material',
    component: MaterialComponent,
    data: {title: 'DOOR | Quản lý chất liệu'},
  },
  {
    path: 'product',
    component: ProductComponent,
    data: {title: 'DOOR | Quản lý sản phẩm'},
  },
  {
    path: 'product/add',
    component: AddOrEditProductComponent,
    data: {title: 'DOOR | Thêm sản phẩm'},
  },
  {
    path: 'product/edit/:id',
    component: AddOrEditProductComponent,
    data: {title: 'DOOR | Sửa sản phẩm'},
  },
  {
    path: 'user',
    component: UserComponent,
    data: {title: 'DOOR | Quản lý tài khoản'},
  },
  {
    path: 'manage-image',
    component: ManageImageComponent,
    data: {title: 'DOOR | Quản lý ảnh'},
  },
];
