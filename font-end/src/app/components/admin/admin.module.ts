import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CategoryComponent} from './category/category.component';
import {ColorComponent} from './color/color.component';
import {RouterModule} from "@angular/router";
import {AdminRoutes} from "./admin-routing.module";
import {ProfileComponent} from './profile/profile.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {LayoutModule} from "../layout/layout.module";
import {MaterialComponent} from './material/material.component';
import {ProductComponent} from './product/product.component';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {UserComponent} from './user/user.component';
import {NgxLoadingModule} from "ngx-loading";
import {AddOrEditProductComponent} from './product/components/add-or-edit-product/add-or-edit-product.component';
import {ManageImageComponent} from './manage-image/manage-image.component';
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {TokenService} from "../../services/token.service";

@NgModule({
  declarations: [
    CategoryComponent,
    ColorComponent,
    ProfileComponent,
    MaterialComponent,
    ProductComponent,
    UserComponent,
    AddOrEditProductComponent,
    ManageImageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(AdminRoutes),
    ReactiveFormsModule,
    FormsModule,
    LayoutModule,
    CKEditorModule,
    NgxLoadingModule
  ]
})
export class AdminModule {
}
