import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../../../models/user";
import {ToastrService} from "ngx-toastr";
import {UserService} from "../../../services/user.service";
import {debounce} from 'lodash';
import swal from "sweetalert2";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  userForm: FormGroup;
  pagination = {
    current_page: null,
    last_page: null
  };
  searchForm: FormGroup;
  users: Array<User> = [];
  user = {
    image: null,
    name: null,
    email: null,
    phone: null,
    address: null,
    birth: null,
    gender: null,
    created_at: null
  };
  submitted = false;
  showAddEdit: boolean;
  addOrEdit: boolean;
  showSearch: boolean;
  showDetail: boolean;
  loading: boolean;
  validateEmail: boolean;
  validatePassword: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private toast: ToastrService,
  ) {
  }

  buildForm(data?) {
    const numbers = /^[+-]?((\.\d+)|(\d+(\.\d+)?))$/;
    if (this.addOrEdit) {
      this.userForm = this.formBuilder.group({
        profile: [false],
        id: [data ? data.id : null],
        name: [data ? data.name : null, [Validators.required, Validators.maxLength(255)]],
        email: [data ? data.email : null, [Validators.required, Validators.maxLength(255)]],
        phone: [data ? data.phone : null, [Validators.required, Validators.maxLength(255), Validators.pattern(numbers), Validators.min(0)]],
        status: [data ? data.status : 1],
        gender: [data ? data.gender : 2],
        image: [data ? data.image : null],
        birth: [data ? data.birth : null],
        address: [data ? data.address : null, [Validators.required, Validators.maxLength(255)]],
      });
    } else {
      this.userForm = this.formBuilder.group({
        profile: [false],
        id: [data ? data.id : null],
        name: [data ? data.name : null, [Validators.required, Validators.maxLength(255)]],
        email: [data ? data.email : null, [Validators.required, Validators.maxLength(255)]],
        phone: [data ? data.phone : null, [Validators.required, Validators.maxLength(255), Validators.pattern(numbers), Validators.min(0)]],
        status: [data ? data.status : 1],
        gender: [data ? data.gender : 2],
        image: [data ? data.image : null],
        birth: [data ? data.birth : null],
        address: [data ? data.address : null, [Validators.required, Validators.maxLength(255)]],
        password: [null, [Validators.required, Validators.maxLength(255), Validators.minLength(6)]]
      });
    }
  }

  buildFormSearch() {
    this.searchForm = this.formBuilder.group({
      name: [''],
      phone: [''],
      email: [''],
      status: [''],
      page: [1],
    })
  }

  get userFormControl() {
    return this.userForm.controls;
  }

  ngOnInit(): void {
    this.buildFormSearch();
    this.list();
  }

  list() {
    this.loading = true;
    this.userService.list(this.searchForm.value).subscribe((res: any) => {
      this.users = res.data.map(val => {
        return new User(val);
      });
      this.pagination.current_page = res.current_page;
      this.pagination.last_page = res.last_page;
      this.loading = false;

    }, (error) => {
      this.toast.error(error, 'Lỗi!!!');
    });
    this.close();
  }

  search() {
    this.showSearch = true;
  }

  addAndEdit(item?) {
    this.showAddEdit = true;
    this.addOrEdit = !!item;
    this.buildForm(item);
  }

  detail(item) {
    this.showDetail = true;
    this.user = item;
  }

  save() {
    this.submitted = true;
    if (this.userForm.valid && this.validateEmail === false && this.validatePassword === false) {
      const apiCall = this.addOrEdit ? this.userService.update(this.userForm.value.id, this.userForm.value) : this.userService.add(this.userForm.value);
      apiCall.subscribe((res) => {
        const title = this.addOrEdit ? 'Sửa tài khoản thành công' : 'Thêm tài khoản thành công';
        this.toast.success(title, 'Thành công');
        this.list();
        this.close();
      }, (error) => {
        if (error.status === 400) {
          Object.keys(error.error).map(key => {
            this.toast.warning(error.error[key], 'Cảnh báo!!!');
          });
        } else {
          this.toast.error('Có lỗi xảy ra', 'Lỗi!!!');
        }
      })
    }
  }

  delete(id: number) {
    swal.fire({
      title: 'Bạn chắc chắn muốn xóa không?',
      text: null,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Xóa',
      cancelButtonText: 'Đóng'
    }).then((result) => {
      if (result.value) {
        this.userService.delete(id).subscribe(() => {
          swal.fire(
            'Xóa thành công',
            '',
            'success'
          );
          this.list();
        }, (error) => {
          swal.fire(
            'Lỗi',
            error,
            'error'
          );
        })
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal.fire(
          'Đóng',
          '',
          'error'
        )
      }
    })
  }

  page(event) {
    this.searchForm.get('page').setValue(event);
    this.list();
  }

  load() {
    this.buildFormSearch();
    this.list();
    this.close();
  }

  checkEmail(event) {
    let email = event.target.value.trim();
    if (email || email != '' || email != null) {
      this.userService.checkEmail({
        id: this.userForm.get('id').value,
        email: event.target.value
      }).subscribe(() => {
        this.validateEmail = false;
      }, () => {
        this.validateEmail = true;
      })
    } else this.validateEmail = false;
  }

  checkPassword(event) {
    let password = event.target.value.trim();
    if (password || password != '' || password != null) {
      const check = debounce(() => {
        this.validatePassword = event.target.value !== this.userForm.get('password').value;
      }, 500);
      check();
    } else this.validatePassword = false;
  }

  close() {
    this.userForm = undefined;
    this.submitted = false;
    this.showAddEdit = false;
    this.showSearch = false;
    this.validateEmail = false;
    this.validatePassword = false;
    this.showDetail = false;
    this.user = undefined;
  }


}
