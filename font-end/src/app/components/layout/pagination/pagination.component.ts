import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input() current_page = 1;
  @Input() last_page: number = null;
  @Output() page = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  increaseAndDecreasePagination(type) {
    let number = 1;
    if (type === 'increase' && this.current_page < this.last_page) {
      number = this.current_page + 1;
    } else if (type === 'decrease' && this.current_page > 1) {
      number = this.current_page - 1;
    }
    this.page.emit(number);
  }

}
