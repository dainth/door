import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LayoutAdminComponent} from './layout-admin/layout-admin.component';
import {LayoutAuthComponent} from './layout-auth/layout-auth.component';
import {RouterModule} from '@angular/router';
import {LinkComponent} from './layout-admin/link/link.component';
import { PaginationComponent } from './pagination/pagination.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { LayoutClientComponent } from './layout-client/layout-client.component';


@NgModule({
  declarations: [
    LayoutAdminComponent,
    LayoutAuthComponent,
    LinkComponent,
    PaginationComponent,
    LayoutClientComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    LayoutAdminComponent,
    LayoutAuthComponent,
    PaginationComponent
  ]
})
export class LayoutModule {
}
