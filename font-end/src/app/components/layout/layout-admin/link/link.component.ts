import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss']
})
export class LinkComponent implements OnInit {

  @Output() changePassword = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit(): void {
  }

  clickChangePassword() {
    this.changePassword.emit(true);
  }

}
