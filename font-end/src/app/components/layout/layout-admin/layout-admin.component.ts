import {Component, OnInit, ViewChild} from '@angular/core';
import {fromEvent} from "rxjs";
import {AuthService} from "../../../services/auth.service";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import swal from "sweetalert2";
import {UserService} from "../../../services/user.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-layout-admin',
  templateUrl: './layout-admin.component.html',
  styleUrls: ['./layout-admin.component.scss']
})
export class LayoutAdminComponent implements OnInit {

  @ViewChild('dropdown') dropdown;
  @ViewChild('dropdownMenu') dropdownMenu;

  imageDefault = environment.imageUserDefault;
  clickOutSub: any;
  user: any;
  changePassword: boolean;
  checkOldPassword: boolean;
  checkNewPasswordConfirmation: boolean;
  submitted: boolean;
  passwordForm: FormGroup;

  constructor(
    private authService: AuthService,
    private toast: ToastrService,
    private router: Router,
    private userService: UserService,
    private formBuilder: FormBuilder,
  ) {
  }

  buildFormPassword() {
    this.passwordForm = this.formBuilder.group({
      password: [null]
    })
  }

  get passwordFormControl() {
    return this.passwordForm.controls;
  }


  ngOnInit(): void {
    this.user = this.authService.getUserLocalStorage();
    if (!this.user) {
      this.router.navigate(['/auth/login']);
    }
  }

  clickDropdown() {
    const dropdown = this.dropdown.nativeElement;
    if (dropdown) {
      const dropdownMenu = this.dropdownMenu.nativeElement;
      if (dropdownMenu.classList.contains('show')) {
        dropdownMenu.classList.remove('show');
        if (this.clickOutSub) {
          this.clickOutSub.unsubscribe();
        }
      } else {
        dropdownMenu.classList.add('show');
        this.clickOutSub = fromEvent(document.body, 'mousedown').subscribe((ev: MouseEvent) => {
          if (!dropdown.contains(ev.target)) {
            dropdownMenu.classList.remove('show');
            this.clickOutSub.unsubscribe();
          }
        });
      }
    }
  }

  clickLogOut() {
    swal.fire({
      title: 'Bạn chắc chắn muốn đăng xuất?',
      text: null,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Đồng ý',
      cancelButtonText: 'Đóng'
    }).then((result) => {
      if (result.value) {
        this.authService.logout().subscribe(() => {
          this.toast.success('Đăng xuất thành công', 'Thông báo');
          this.authService.removeLocalStorage();
          this.router.navigate(['/auth/login']);
        }, () => {
          swal.fire(
            'Lỗi!!!',
            'Có lỗi xảy ra',
            'error'
          );
        })
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swal.fire(
          'Đóng',
          '',
          'error'
        )
      }
    })
  }

  clickChangePassword(event?) {
    this.buildFormPassword();
    this.changePassword = !this.changePassword;
    if (event === undefined) this.clickDropdown();
  }

  close() {
    this.changePassword = false;
    this.checkOldPassword = false;
    this.checkNewPasswordConfirmation = false;
    this.submitted = false;
    this.passwordForm = undefined;
  }

  checkPassword(event, type) {
    let password = event.target.value.trim();
    if (type === 'old_password') {
      if (password || password != '' || password != null) {
        this.userService.checkPassword({password: password}).subscribe(() => {
          this.checkOldPassword = false;
        }, () => {
          this.checkOldPassword = true;
        })
      }
    } else {
      if (password || password != '' || password != null) {
        if (password === this.passwordForm.get('password').value) this.checkNewPasswordConfirmation = false;
        else this.checkNewPasswordConfirmation = true;
      }
    }
  }

  save() {
    this.submitted = true;
    if (this.passwordForm.valid && this.checkOldPassword === false && this.checkNewPasswordConfirmation === false) {
      this.userService.updatePassword(this.passwordForm.value).subscribe((res) => {
        this.toast.success('Đổi mật khẩu thành  công', 'Thành công');
        this.router.navigate(['/auth/login']);
        this.close();
      }, (error) => {
        if (error.status === 400) {
          Object.keys(error.error).map(key => {
            this.toast.warning(error.error[key], 'Cảnh báo!!!');
          });
        } else {
          this.toast.error('Có lỗi xảy ra', 'Lỗi!!!');
        }
      })
    }
  }

}
