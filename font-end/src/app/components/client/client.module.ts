import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {ClientRoutes} from "./client-routing.module";
import { HomeComponent } from './components/home/home.component';
import { DetailComponent } from './components/detail/detail.component';
import { CategoryComponent } from './components/category/category.component';
import { ContactComponent } from './components/contact/contact.component';


@NgModule({
  declarations: [HomeComponent, DetailComponent, CategoryComponent, ContactComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ClientRoutes),
  ]
})
export class ClientModule {
}
