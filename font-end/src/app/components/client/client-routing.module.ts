import {Routes} from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {CategoryComponent} from "./components/category/category.component";
import {DetailComponent} from "./components/detail/detail.component";
import {ContactComponent} from "./components/contact/contact.component";

export const ClientRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {title: 'DOOR | Mua sản phẩm thuận tiện nhất'}
  },
  {
    path: 'danh-muc/:slug',
    component: CategoryComponent,
    data: {title: 'DOOR | Danh mục sản phẩm'}
  },
  {
    path: 'san-pham/:slug',
    component: DetailComponent,
    data: {title: 'DOOR | Chi tết sản phẩm'}
  },
  {
    path: 'lien-he',
    component: ContactComponent,
    data: {title: 'DOOR | Liên hệ'}
  },
];
