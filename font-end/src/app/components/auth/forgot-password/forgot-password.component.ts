import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {AuthService} from "../../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['../login/login.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  forgotPasswordForm: FormGroup;
  submitted = false;
  loading = false;
  forgotOrVerify = false;
  validateToken = false;
  verifyPasswordConfirmation = false;

  constructor(
    private formBuilder: FormBuilder,
    private toast: ToastrService,
    private authService: AuthService,
    public router: Router,
  ) {
  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.forgotPasswordForm = this.formBuilder.group({
      email: [null, [Validators.required, Validators.maxLength(255)]],
      token: [null, this.forgotOrVerify ? [Validators.required, Validators.maxLength(255)] : false],
      password: [null, this.forgotOrVerify ? [Validators.required, Validators.maxLength(255), Validators.minLength(6)] : false],
    });
  }

  get forgotPasswordFormControl() {
    return this.forgotPasswordForm.controls;
  }

  forgotPassword() {
    this.submitted = true;
    if (this.forgotPasswordForm.valid) {
      this.loading = true;
      this.authService.forgotPassword(this.forgotPasswordForm.value).subscribe((res: any) => {
        switch (res.message) {
          case 'Tài khoản không còn tồn tại!!!' :
            this.toast.warning('Tài khoản không còn tồn tại!!!', 'Cảnh báo');
            break;
          case 'Thông tin tài khoản hoặc mật khẩu không đúng!!!':
            this.toast.warning('Thông tin tài khoản hoặc mật khẩu không đúng!!!', 'Cảnh báo');
            break;
          case 'Tài khoản đăng nhập không còn hoạt động!!!':
            this.toast.warning('Tài khoản đăng nhập không còn hoạt động!!!', 'Cảnh báo');
            break;
          case 'Mã xác thực đã được gửi đến email, vui lòng kiểm tra hộp thư':
            this.toast.success('Mã xác thực đã được gửi đến email, vui lòng kiểm tra hộp thư', 'Thành cồng');
            this.submitted = false;
            this.forgotOrVerify = true;
            break;
          default:
            this.toast.error('Có lỗi xảy ra!!!', 'Lỗi');
        }
        this.loading = false;
      }, () => {
        this.toast.error('Có lỗi xảy ra!!!', 'Lỗi');
        this.loading = false;
      });
    }
  }

  verifyPassword() {
    this.submitted = true;
    if (this.forgotPasswordForm.valid && this.verifyPasswordConfirmation === false) {
      this.loading = true;
      this.authService.verifyPassword(this.forgotPasswordForm.value).subscribe((res: any) => {
        switch (res.message) {
          case 'Vui lòng nhập lại email hoặc mã xác thực' :
            this.toast.warning('Vui lòng nhập lại email hoặc mã xác thực', 'Cảnh báo');
            break;
          case 'Mã xác thực đã hết hạn':
            this.toast.warning('Mã xác thực đã hết hạn', 'Cảnh báo');
            break;
          case 'Cập nhật mật khẩu thành công':
            this.toast.success('Cập nhật mật khẩu thành công', 'Thành cồng');
            this.router.navigate(['/auth/login']);
            break;
          default:
            this.toast.error('Có lỗi xảy ra!!!', 'Lỗi');
        }
        this.loading = false;
      }, () => {
        this.toast.error('Có lỗi xảy ra!!!', 'Lỗi');
        this.loading = false;
      });
    }
  }

  checkPassword(event) {
    let password = event.target.value.trim();

    if (password || password != '' || password != null) {
      if (password === this.forgotPasswordForm.get('password').value) this.verifyPasswordConfirmation = false;
      else this.verifyPasswordConfirmation = true;
    }
  }

  prev() {
    this.forgotOrVerify = false;
    this.verifyPasswordConfirmation = false;
  }
}
