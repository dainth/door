import {Routes} from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {ForgotPasswordComponent} from "./forgot-password/forgot-password.component";

export const AuthRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    data: {title: 'DOOR | Đăng nhập'}
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent,
    data: {title: 'DOOR | Quên mật khẩu'}
  },
];
