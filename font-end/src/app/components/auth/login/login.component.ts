import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  body = {
    email: null,
    password: null,
    remember_me: false
  };
  loading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private toast: ToastrService,
    private authService: AuthService,
    public router: Router,
  ) {
  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.loginForm = this.formBuilder.group({
      email: [null, [Validators.required, Validators.maxLength(255)]],
      password: [null, [Validators.required, Validators.maxLength(255), Validators.minLength(6)]],
      remember_me: true
    });
  }

  get loginFormControl() {
    return this.loginForm.controls;
  }

  login() {
    this.submitted = true;
    if (this.loginForm.valid) {
      this.loading = true;
      this.authService.login(this.loginForm.value).subscribe((res: any) => {
        this.authService.setTokenLocalStorage(res);

        this.authService.me().subscribe((item: any) => {
          this.toast.success('Đăng nhập thành công', 'Thông báo');
          this.authService.setUserLocalStorage(item);
          this.submitted = false;

          this.router.navigate(['/admin'], {replaceUrl: true});
        }, (error: any) => {
          this.toast.error('Có lỗi xảy ra, vui lòng đăng nhập lại', 'Lỗi!!!');
          this.authService.removeLocalStorage();
          this.loading = false;
        });

      }, (error: any) => {
        Object.keys(error.error).map(key => {
          this.toast.error(error.error[key], 'Lỗi!!!');
        });

        this.authService.removeLocalStorage();
        this.loading = false;
      });
    }
  }
}
