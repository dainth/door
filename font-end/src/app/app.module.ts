import {BrowserModule, Title} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AdminModule} from './components/admin/admin.module';
import {AppRoutes, CanActiveAdmin} from './app-routing.module';
import {AuthModule} from './components/auth/auth.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToastrModule} from 'ngx-toastr';
import {RouterModule} from '@angular/router';
import {LayoutModule} from './components/layout/layout.module';
import {NgxLoadingModule, ngxLoadingAnimationTypes} from 'ngx-loading';
import {TokenService} from "./services/token.service";
import {ClientModule} from "./components/client/client.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AdminModule,
    RouterModule.forRoot(AppRoutes),
    AuthModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    ClientModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-top-right'
    }),
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.circle,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)',
      primaryColour: '#ffffff',
      secondaryColour: '#ff0002',
    })
  ],
  providers: [
    Title,
    CanActiveAdmin,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenService,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
