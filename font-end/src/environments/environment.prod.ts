export const environment = {
  production: true,
  baseUrlApi: 'http://127.0.0.1:8000/api/',
  imageDefault : '/angular/assets/icon/no-image.jpg',
  imageUserDefault : '/angular/assets/icon/logo_user.png',
};
