<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdToOptionProductsTable extends Migration
{
    public function up()
    {
        Schema::table('option_products', function (Blueprint $table) {
            $table->id()->first();
        });
    }

    public function down()
    {
        Schema::table('option_products', function (Blueprint $table) {
            //
        });
    }
}
