<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Illuminate\Database\Eloquent\Model::unguard();
        DB::statement("SET FOREIGN_KEY_CHECKS = 0");
        $tables = DB::select('SHOW TABLES');
        $db_name = config('database.connections.mysql.database');
        foreach ($tables as $table) {
            if ($table->{'Tables_in_' . $db_name} !== 'migrations')
                DB::table($table->{'Tables_in_' . $db_name})->truncate();
        }
        DB::statement("SET FOREIGN_KEY_CHECKS = 1");

        $this->call(UserSeeder::class);

//        $faker = Faker\Factory::create();
//
//        $limit = 200;
//
//        for ($i = 0; $i < $limit; $i++) {
//            \App\Models\Category::create([
//                'name' => $faker->name,
//                'slug' => $faker->slug,
//                'status' => 1
//            ]);
//        }
//
//        for ($i = 0; $i < $limit; $i++) {
//            \App\Models\Material::create([
//                'name' => $faker->name,
//            ]);
//        }
    }
}
