<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        \App\Models\User::create([
            'name' => 'Admin',
            'address' => "Thái Bình",
            'gender' => 2,
            'status' => 2,
            'email' => 'door@gmail.com',
            'password' => \Illuminate\Support\Facades\Hash::make('door12345!'),
        ]);
    }
}
